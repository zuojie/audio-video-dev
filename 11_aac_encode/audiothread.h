#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H
#include <QThread>
#include "ffmpegutil.h"

class AudioThread : public QThread
{

private:
    void run();

public:
    explicit AudioThread(QObject *parent = nullptr);
    ~AudioThread();
};

#endif // AUDIOTHREAD_H
