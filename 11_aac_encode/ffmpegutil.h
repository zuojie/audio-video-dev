#ifndef FFMPEGUTIL_H
#define FFMPEGUTIL_H

#include <QDebug>
#include <QFile>

extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
}

typedef struct {
    const char *filename;
    int sampleRate;
    AVSampleFormat sampleFmt;
    int chLayout;
} AudioEncodeSpec;

class FFmpegUtil
{
public:
    FFmpegUtil();
    static void aacEncode(AudioEncodeSpec &in,
                          const char *outFilename);
};

#endif // FFMPEGUTIL_H
