#ifndef FFMPEGUTILS_H
#define FFMPEGUTILS_H
#define __STDC_CONSTANT_MACROS

extern "C"{
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

typedef struct {
    char *pixels;
    int width;
    int height;
    AVPixelFormat format;
} RawVideoFrame;

typedef struct {
    const char *filename;
    int width;
    int height;
    AVPixelFormat format;
} RawVideoFile;

class FFmpegUtils
{
public:
    FFmpegUtils();
    static void convertRawVideo(RawVideoFrame &in, RawVideoFrame &out);
    static void convertRawVideo(RawVideoFile &in, RawVideoFile &out);
};

#endif // FFMPEGUTILS_H
