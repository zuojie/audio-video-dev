#include "videothread.h"
#include "demuxer.h"

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    #define IN_FILENAME "../test/demuxer/in.mp4"
    #define AOUT_FILENAME "../test/demuxer/out.pcm"
    #define VOUT_FILENAME "../test/demuxer/out.yuv"
#else
    #define IN_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/demuxer/in.mp4"
    #define AOUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/demuxer/out.pcm"
    #define VOUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/demuxer/out.yuv"
#endif

extern "C"{
#include <libavutil/imgutils.h>
}

VideoThread::VideoThread(QObject *parent):QThread(parent)
{
    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &VideoThread::finished,
                this, &VideoThread::deleteLater);
}

VideoThread::~VideoThread(){
    // 断开所有的连接
    disconnect();
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void VideoThread::run(){
    AudioDecodeSpec aOut;
    aOut.filename = AOUT_FILENAME;

    VideoDecodeSpec vOut;
    vOut.filename = VOUT_FILENAME;

    Demuxer().demux(IN_FILENAME,aOut,vOut);
//    FFmpegUtils::h264Decode(IN_FILENAME,out);
    qDebug() << aOut.sampleRate << av_get_channel_layout_nb_channels(aOut.chLayout)
             << av_get_sample_fmt_name(aOut.sampleFmt);

    qDebug() <<vOut.width <<vOut.height << vOut.fps << av_get_pix_fmt_name(vOut.pixFmt);
}
