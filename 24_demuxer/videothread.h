#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H
#include <QThread>
#include <QDebug>

class VideoThread : public QThread
{

private:
    void run();

public:
    explicit VideoThread(QObject *parent = nullptr);
    ~VideoThread();
};

#endif // AUDIOTHREAD_H
