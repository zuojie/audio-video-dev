#include "videothread.h"

extern "C" {
    // 设备相关API
    #include <libavdevice/avdevice.h>
    // 格式相关API
    #include <libavformat/avformat.h>
    // 工具相关API（比如错误处理）
    #include <libavutil/avutil.h>
    #include <libavutil/imgutils.h>
    // 编码相关API
    #include <libavcodec/avcodec.h>
}

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    // 格式名称
    #define FMT_NAME "dshow"
    // 设备名称
    #define DEVICE_NAME "video=UVC Camera"
    // PCM文件的文件名
    #define FILENAME "../test/out.yuv"
#else
    #define FMT_NAME "avfoundation"
    #define DEVICE_NAME "1"
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/out.yuv"
#endif

VideoThread::VideoThread(QObject *parent):QThread(parent)
{
    qDebug() << av_version_info();
    // 初始化libavdevice并注册所有输入和输出设备
    avdevice_register_all();

    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &VideoThread::finished,
                this, &VideoThread::deleteLater);
}

VideoThread::~VideoThread(){
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void VideoThread::run(){
    AVInputFormat *fmt = av_find_input_format(FMT_NAME);
    if(!fmt){
        // 如果找不到输入格式
        qDebug() << "av_find_input_format error" << FMT_NAME;
        return;
    }

    AVFormatContext *ctx = nullptr;
    AVDictionary *options = nullptr;
    av_dict_set(&options,"video_size","640x480",0);
    av_dict_set(&options,"pixel_format", "yuyv422", 0);
    av_dict_set(&options, "framerate", "30", 0);
    int ret = avformat_open_input(&ctx,DEVICE_NAME,fmt,&options);

    if(ret < 0){
         char errbuf[1024] = {0};
        av_strerror(ret,errbuf,sizeof(errbuf));
        qDebug() << "avformat_open_input error" << errbuf;
        return;
    }


    QFile file(FILENAME);
    // WriteOnly：只写模式。如果文件不存在，就创建文件；如果文件存在，就删除文件内容
    if (!file.open(QFile::WriteOnly)) {
        qDebug() << "file open error" << FILENAME;
        // 关闭设备
        avformat_close_input(&ctx);
        return;
    }

    // 写入WAV文件头
    // 获取输入流
    AVStream *stream = ctx->streams[0];
    // 获取视频频参数
    AVCodecParameters *params = stream->codecpar;

    // 计算每一帧的大小
    int imageSize = av_image_get_buffer_size(
                        (AVPixelFormat) params->format,
                        params->width, params->height,
                        1);
    qDebug() << "imageSize:"<<imageSize;

//    AVPixelFormat pixFmt = (AVPixelFormat) params->format;
//    qDebug() <<"pixFmt:"<<pixFmt <<",width:"<< params->width <<",height:"<<params->height;
//    qDebug() << av_pix_fmt_desc_get(pixFmt)->name;
//    int pixSize = av_get_bits_per_pixel(av_pix_fmt_desc_get(pixFmt)) >> 3;
//    int imageSize = params->width * params->height * pixSize;
//    qDebug() << "pixSize:"<<pixSize<<",imageSize:"<<imageSize;

    // 数据包
    AVPacket *pkt = av_packet_alloc();
    while (!isInterruptionRequested()) {
        // 从设备中采集数据，返回值为0，代表采集数据成功
        ret = av_read_frame(ctx, pkt);
        if (ret == 0) { // 读取成功
            // 将数据写入文件
            file.write((const char *) pkt->data, imageSize);
            /*
             这里要使用imageSize，而不是pkt->size。
             pkt->size有可能比imageSize大（比如在Mac平台），
             使用pkt->size会导致写入一些多余数据到YUV文件中，
             进而导致YUV内容无法正常播放
            */

            // 释放资源
            av_packet_unref(pkt);
        } else if (ret == AVERROR(EAGAIN)) { // 资源临时不可用
//            qDebug() << "资源临时不可用：" << ret;
            continue;
        } else { // 其他错误
            char errbuf[1024];
            av_strerror(ret, errbuf, sizeof (errbuf));
            qDebug() << "av_read_frame error" << errbuf << ret;
            break;
        }
    }

    // 关闭文件
    file.close();

    // 释放资源
    av_packet_unref(pkt);
    // 关闭设备
    avformat_close_input(&ctx);

    qDebug() << this << "---正常结束---";
}
