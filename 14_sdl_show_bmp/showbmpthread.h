#ifndef SHOWBMPTHREAD_H
#define SHOWBMPTHREAD_H

#include <QThread>
#include <QDebug>

class ShowBmpThread : public QThread
{
    Q_OBJECT
private:
    void run() override;
public:
    explicit ShowBmpThread(QObject *parent = nullptr);
    ~ShowBmpThread();
signals:

};

#endif // SHOWBMPTHREAD_H
