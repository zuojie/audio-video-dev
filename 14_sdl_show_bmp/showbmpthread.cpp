#include "showbmpthread.h"
#include <SDL2/SDL.h>
#include <QFile>

#ifdef Q_OS_WIN
    #define FILENAME "../test/in.bmp"
#else
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/in.bmp"
#endif

// 出错了就执行goto end
#define END(judge, func) \
    if (judge) { \
        qDebug() << #func << "Error" << SDL_GetError(); \
        goto end; \
    }

ShowBmpThread::ShowBmpThread(QObject *parent) : QThread(parent){
    connect(this,&ShowBmpThread::finished,&ShowBmpThread::deleteLater);
}

ShowBmpThread::~ShowBmpThread(){
    disconnect();
    requestInterruption();
    quit();
    wait();
    qDebug() << this << "析构了";
}

void ShowBmpThread::run(){
    //定义变量
    // 窗口
    SDL_Window *window = nullptr;
    // 渲染上下文
    SDL_Renderer *renderer = nullptr;
    // 像素数据
    SDL_Surface *surface = nullptr;
    // 纹理（直接跟特定驱动程序相关的像素数据）
    SDL_Texture *texture = nullptr;

    // 初始化Video子系统
    END(SDL_Init(SDL_INIT_VIDEO),SDL_Init);

    // 加载BMP
    surface = SDL_LoadBMP(FILENAME);
    END(!surface, SDL_LoadBMP);

    // 创建窗口
    window  = SDL_CreateWindow(
                // 窗口标题
                "SDL显示BMP图片",
                // 窗口的x坐标（未定义）
                SDL_WINDOWPOS_UNDEFINED,
                // 窗口的y坐标（未定义）
                SDL_WINDOWPOS_UNDEFINED,
                // 窗口宽度（跟图片宽度一样）
                surface->w,
                // 窗口高度（跟图片高度一样）
                surface->h,
                // SDL_WindowFlags 枚举，设置窗口显示样式效果
                SDL_WINDOW_SHOWN);

    // 创建渲染上下文（默认的渲染目标是window）
    renderer = SDL_CreateRenderer(window,
                       // 要初始化的渲染设备的索引，设置 -1 则初始化第一个支持 flags 的设备
                       -1,
                       SDL_RENDERER_ACCELERATED |
                       SDL_RENDERER_PRESENTVSYNC);
    if (!renderer) { // 说明开启硬件加速失败
        renderer = SDL_CreateRenderer(window, -1, 0);
    }
    END(!renderer, SDL_CreateRenderer);

    // 创建纹理
    texture = SDL_CreateTextureFromSurface(renderer,surface);
    END(!texture, SDL_CreateTextureFromSurface);

    // 渲染
    // 设置绘制颜色（这里随便设置了一个颜色：黄色）
    END(SDL_SetRenderDrawColor(renderer,255,255,0,SDL_ALPHA_OPAQUE),SDL_SetRenderDrawColor);

    // 用DrawColor清除渲染目标
    END(SDL_RenderClear(renderer),SDL_RenderClear);

    // 复制纹理到渲染目标上（默认是window）可以使用SDL_SetRenderTarget()修改渲染目标
    // srcrect源矩形框，dstrect目标矩形框，两者都传nullptr表示整个纹理渲染到整个目标上去
    END(SDL_RenderCopy(renderer,texture,nullptr,nullptr),SDL_RenderCopy);

    // 将此前的所有需要渲染的内容更新到屏幕上
    SDL_RenderPresent(renderer);

    // 延迟3秒退出
//    SDL_Delay(3000);

    while (!isInterruptionRequested()) {
        SDL_Event event;
        SDL_WaitEvent(&event);
        switch (event.type) {
            case SDL_QUIT:
                goto end;
        }
    }
end:
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

