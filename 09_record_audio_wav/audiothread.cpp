#include "audiothread.h"
#include "ffmpegutil.h"

extern "C" {
    // 设备相关API
    #include <libavdevice/avdevice.h>
    // 格式相关API
    #include <libavformat/avformat.h>
    // 工具相关API（比如错误处理）
    #include <libavutil/avutil.h>
    // 编码相关API
    #include <libavcodec/avcodec.h>
}

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    // 格式名称
    #define FMT_NAME "dshow"
    // 设备名称
//    #define DEVICE_NAME "audio=麦克风 (Realtek(R) Audio)"
    #define DEVICE_NAME "audio=外接麦克风 (Synaptics HD Audio)"
    // PCM文件的文件名
    #define FILENAME "../"
#else
    #define FMT_NAME "avfoundation"
    #define DEVICE_NAME ":0"
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/"
#endif

AudioThread::AudioThread(QObject *parent):QThread(parent)
{
    qDebug() << av_version_info();
    // 初始化libavdevice并注册所有输入和输出设备
    avdevice_register_all();

    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &AudioThread::finished,
                this, &AudioThread::deleteLater);
}

AudioThread::~AudioThread(){
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

// 从AVFormatContext中获取录音设备的相关参数
void showSpec(AVFormatContext *ctx) {
    // 获取输入流
    AVStream *stream = ctx->streams[0];
    // 获取音频参数
    AVCodecParameters *params = stream->codecpar;
    // 声道数
    qDebug() << "声道数:" << params->channels;
    // 采样率
    qDebug() << "采样率:" << params->sample_rate;
    // 采样格式
    qDebug() << "采样格式:" << params->format;
    // 每一个样本的一个声道占用多少个字节
    qDebug() << "采样格式,一个声道占用:" << av_get_bytes_per_sample((AVSampleFormat) params->format)<<"字节";
    // 编码ID（可以看出采样格式）
    qDebug() << "编码ID:" << params->codec_id;
    // 每一个样本的一个声道占用多少位（这个函数需要用到avcodec库）
    qDebug() << "采样格式:" << av_get_bits_per_sample(params->codec_id)<<"位";
}

void AudioThread::run(){
    AVInputFormat *fmt = av_find_input_format(FMT_NAME);
    if(!fmt){
        // 如果找不到输入格式
        qDebug() << "找不到输入格式" << FMT_NAME;
        return;
    }

    AVFormatContext *ctx = nullptr;
    AVDictionary *options = nullptr;
    int ret = avformat_open_input(&ctx,DEVICE_NAME,fmt,&options);

    if(ret < 0){
         char errbuf[1024] = {0};
        av_strerror(ret,errbuf,sizeof(errbuf));
        qDebug() << "打开设备失败" << errbuf;
        return;
    }

    showSpec(ctx);

    QString fileName = FILENAME;
    fileName += QDateTime::currentDateTime().toString("MM_dd_HH_mm_ss");
    fileName += ".wav";
    QFile file(fileName);
    // WriteOnly：只写模式。如果文件不存在，就创建文件；如果文件存在，就删除文件内容
    if (!file.open(QFile::WriteOnly)) {
        qDebug() << "文件打开失败" << FILENAME;
        // 关闭设备
        avformat_close_input(&ctx);
        return;
    }

    // 写入WAV文件头
    // 获取输入流
    AVStream *stream = ctx->streams[0];
    // 获取音频参数
    AVCodecParameters *params = stream->codecpar;

    // pcm转wav文件
    WAVHeader header;
    header.numChannels = params->channels;
    header.sampleRate = params->sample_rate;
    header.bitsPerSample = av_get_bits_per_sample(params->codec_id);
    if(params->codec_id >= AV_CODEC_ID_PCM_F32BE){
        header.audioFormat = AUDIO_FORMAT_FLOAT;
    }

    header.blockAlign = header.bitsPerSample * header.numChannels >> 3;
    header.byteRate = header.sampleRate *  header.blockAlign;
//    header.dataChunkDataSize = 0;
    file.write((char *)&header,sizeof (WAVHeader));


    // 数据包
    AVPacket pkt;
    while (!isInterruptionRequested()) {
        // 从设备中采集数据，返回值为0，代表采集数据成功
        ret = av_read_frame(ctx, &pkt);
        if (ret == 0) { // 读取成功
            // 将数据写入文件
            file.write((const char *) pkt.data, pkt.size);
            // 计算录音时长
//            header.dataChunkDataSize += pkt.size;
            // 释放资源
            av_packet_unref(&pkt);
        } else if (ret == AVERROR(EAGAIN)) { // 资源临时不可用
            qDebug() << "资源临时不可用：" << ret;
            continue;
        } else { // 其他错误
            char errbuf[1024];
            av_strerror(ret, errbuf, sizeof (errbuf));
            qDebug() << "av_read_frame error" << errbuf << ret;
            break;
        }
    }

    qDebug() << file.size() << header.dataChunkDataSize;

    // 写入dataChunkDataSize
    header.dataChunkDataSize = file.size() - sizeof (WAVHeader);//pcm数据大小
    file.seek(sizeof (WAVHeader) - sizeof (header.dataChunkDataSize));
    file.write((char *)&header.dataChunkDataSize,sizeof (header.dataChunkDataSize));

    // 写入riffChunkDataSize
//    header.riffChunkDataSize = header.dataChunkDataSize + sizeof (WAVHeader)
//            - sizeof (header.riffChunkId)
//            - sizeof (header.riffChunkDataSize);
    header.riffChunkDataSize = file.size()
            - sizeof (header.riffChunkId)
            - sizeof (header.riffChunkDataSize);
    file.seek(sizeof (header.riffChunkId));
    file.write((char *)&header.riffChunkDataSize,sizeof (header.riffChunkDataSize));

    // 关闭文件
    file.close();

    // 释放资源
    av_packet_unref(&pkt);
    // 关闭设备
    avformat_close_input(&ctx);

    qDebug() << this << "---正常结束---";
}
