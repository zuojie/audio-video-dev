#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "playthread.h">


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
//    SDL_version v;
//    SDL_VERSION(&v);
//    qDebug() << v.major << v.minor << v.patch;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_playButton_clicked()
{
    PlayThread *playThread = new PlayThread(this);
    playThread->start();
}
