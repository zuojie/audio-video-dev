#ifndef SHOWBMPTHREAD_H
#define SHOWBMPTHREAD_H

#include <QThread>
#include <QDebug>

class ShowThread : public QThread
{
    Q_OBJECT
private:
    void run() override;
    void *_winId;

public:
    explicit ShowThread(void *winId,QObject *parent = nullptr);
    ~ShowThread();
signals:

};

#endif // SHOWBMPTHREAD_H
