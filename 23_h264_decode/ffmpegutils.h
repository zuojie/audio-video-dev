#ifndef FFMPEGUTIL_H
#define FFMPEGUTIL_H

#include <QDebug>
#include <QFile>

extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include <libavutil/imgutils.h>
}

typedef struct {
    const char *filename;
    int width;
    int height;
    AVPixelFormat pixFmt;
    int fps;
} VideoDecodeSpec;

class FFmpegUtils
{
public:
    FFmpegUtils();
    static void h264Decode(const char *inFilename,
                          VideoDecodeSpec &out);
};

#endif // FFMPEGUTIL_H
