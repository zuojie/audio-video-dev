#include "videothread.h"

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    #define IN_FILENAME "../test/h264/in.h264"
    #define OUT_FILENAME "../test/h264/out.yuv"
#else
    #define IN_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/h264/in.h264"
    #define OUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/h264/out.yuv"
#endif

extern "C"{
#include <libavutil/imgutils.h>
}

VideoThread::VideoThread(QObject *parent):QThread(parent)
{
    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &VideoThread::finished,
                this, &VideoThread::deleteLater);
}

VideoThread::~VideoThread(){
    // 断开所有的连接
    disconnect();
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void VideoThread::run(){
    VideoDecodeSpec out;
    out.filename = OUT_FILENAME;

    FFmpegUtils::h264Decode(IN_FILENAME,out);

    qDebug() <<out.width <<out.height << out.fps << av_get_pix_fmt_name(out.pixFmt);
}
