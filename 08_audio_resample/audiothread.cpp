#include "audiothread.h"

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    // PCM文件的文件名
    #define IN_FILENAME "../test/44100_s16le_2.pcm"
    #define OUT_FILENAME "../test/48000_f32le_1.pcm"
#else
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test"
    #define IN_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/44100_s16le_2.pcm"
    #define OUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/48000_f32le_1.pcm"
#endif

AudioThread::AudioThread(QObject *parent):QThread(parent)
{
    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &AudioThread::finished,
                this, &AudioThread::deleteLater);
}

AudioThread::~AudioThread(){
    // 断开所有的连接
    disconnect();
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void AudioThread::run(){
    ResampleAudioSpec in;
    in.filename = IN_FILENAME;
    in.chLayout = AV_CH_LAYOUT_STEREO;//双声道
    in.sampleFmt = AV_SAMPLE_FMT_S16;
    in.sampleRate = 44100;

    ResampleAudioSpec out;
    out.filename = OUT_FILENAME;
    out.chLayout = AV_CH_LAYOUT_MONO;//单声道
    out.sampleFmt = AV_SAMPLE_FMT_FLT;
    out.sampleRate = 48000;

    FFmpegUtil::resampleAudio(in,out);
}
