#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H
#include <QThread>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QDateTime>

class AudioThread : public QThread
{
    Q_OBJECT
private:
    void run();

public:
    explicit AudioThread(QObject *parent = nullptr);
    ~AudioThread();
signals:
    void timeChanged(unsigned long long ms);
};

#endif // AUDIOTHREAD_H
