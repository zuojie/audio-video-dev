#ifndef PLAYTHRED_H
#define PLAYTHRED_H

#include <QThread>
#include <QDebug>

class PlayThread : public QThread
{
    Q_OBJECT
private:
    void run() override;
public:
    explicit PlayThread(QObject *parent = nullptr);
    ~PlayThread();
signals:

};

#endif // PLAYTHRED_H
