#include "videothread.h"

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    // PCM文件的文件名
    #define IN_FILENAME "../test/out_640x480.yuv"
    #define OUT_FILENAME "../test/out_640x480.h264"
#else
    #define IN_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/out_640x480.yuv"
    #define OUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/out_640x480.h264"
#endif

VideoThread::VideoThread(QObject *parent):QThread(parent)
{
    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &VideoThread::finished,
                this, &VideoThread::deleteLater);
}

VideoThread::~VideoThread(){
    // 断开所有的连接
    disconnect();
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void VideoThread::run(){
    VideoEncodeSpec in;
    in.filename = IN_FILENAME;
    in.width = 640;
    in.height = 480;
    in.fps = 30;
    in.format = AV_PIX_FMT_YUV420P;

    FFmpegUtils::h264Encode(in,OUT_FILENAME);
}
