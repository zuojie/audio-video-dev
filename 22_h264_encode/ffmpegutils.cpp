#include "ffmpegutils.h"

// 处理错误码
#define ERROR_BUF(ret) \
    char errbuf[1024]; \
    av_strerror(ret, errbuf, sizeof (errbuf));

FFmpegUtils::FFmpegUtils(){}

static int check_pix_fmt(const AVCodec *codec,enum AVPixelFormat pixFmt){
    const enum AVPixelFormat *p = codec->pix_fmts;
    while (*p != AV_PIX_FMT_NONE) {
        if(*p == pixFmt) return 1;
        p++;
    }
    return 0;
}

// 返回0：编码操作正常完成,返回负数：中途出现了错误
static int encode(AVCodecContext *ctx,
                  AVFrame *frame,
                  AVPacket *pkt,
                  QFile &outFile) {
    // 发送数据到编码器
    int ret = avcodec_send_frame(ctx, frame);
    if (ret < 0) {
        ERROR_BUF(ret);
        qDebug() << "avcodec_send_frame error" << errbuf;
        return ret;
    }

    // 不断从编码器中取出编码后的数据
    while (true) {
        // 从编码器中获取编码后的数据
        ret = avcodec_receive_packet(ctx, pkt);
        // packet中已经没有数据，需要重新发送数据到编码器（send frame）
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            return 0;
        } else if (ret < 0) { // 出现了其他错误
            ERROR_BUF(ret);
            qDebug() << "avcodec_receive_packet error" << errbuf;
            return ret;
        }

        // 成功从编码器拿到编码后的数据
        // 将编码后的数据写入文件
        outFile.write((char *) pkt->data, pkt->size);

        // 释放资源
        av_packet_unref(pkt);
    }
    return 0;
}

void FFmpegUtils::h264Encode(VideoEncodeSpec &in, const char *outFilename){
    // 文件
    QFile inFile(in.filename);
    QFile outFile(outFilename);

    // 一帧图片的大小
    int imgSize = av_image_get_buffer_size(in.format,in.width,in.height,1);

    // 返回结果
    int ret = 0;
    // 编码器
    AVCodec *codec = nullptr;

    // 编码上下文
    AVCodecContext *ctx = nullptr;

    // 存放编码前的数据（yuv）
    AVFrame *frame = nullptr;

    // 存放编码后的数据（h264）
    AVPacket *pkt = nullptr;

//    uint8_t *buf = nullptr;

    // 获取编码器
    codec = avcodec_find_encoder(AV_CODEC_ID_H264);
//    codec = avcodec_find_encoder_by_name("libx264");
    qDebug() << "codec.name:" << codec->name<<",codec.id:"<<codec->id;
    if(!codec){
        qDebug() << "encoder not found";
        return;
    }

    // 检查输入数据的采样格式
    if(!check_pix_fmt(codec,in.format)){
        qDebug() << "Encoder does not support pixel format"
                    << av_get_pix_fmt_name(in.format);
        return;
    }

    // 创建上下文
    ctx = avcodec_alloc_context3(codec);
    if (!ctx) {
        qDebug() << "avcodec_alloc_context3 error";
        return;
    }

    // 设置参数
    ctx->width = in.width;
    ctx->height = in.height;
    ctx->pix_fmt = in.format;
    // 设置帧率（1秒钟显示的帧数是in.fps）
    ctx->time_base = {1,in.fps};

    // 打开编码器
    ret = avcodec_open2(ctx,codec,nullptr);
    if (ret < 0) {
        ERROR_BUF(ret);
        qDebug() << "avcodec_open2 error" << errbuf;
        goto end;
    }

    // 创建AVFrame
    frame = av_frame_alloc();
    if (!frame) {
        qDebug() << "av_frame_alloc error";
        goto end;
    }

    frame->width = ctx->width;
    frame->height = ctx->height;
    frame->format = ctx->pix_fmt;
    frame->pts = 0;

    // 创建输入缓冲区 方法一
    ret = av_image_alloc(frame->data,frame->linesize,in.width,in.height,in.format,1);

    // 创建输入缓冲区 方法二
//    buf = (uint8_t *)av_malloc(imgSize);
//    ret = av_image_fill_arrays(frame->data,frame->linesize,
//                               buf,
//                               in.format,in.width,in.height,1);

    // 利用width、height、format创建缓冲区 方法三
//    ret = av_frame_get_buffer(frame, 0);
    if (ret < 0) {
        ERROR_BUF(ret);
        qDebug() << "av_frame_get_buffer error" << errbuf;
        goto end;
    }

//    qDebug() << buf <<frame->data[0];

    // 打印 frame->data：
    qDebug() << frame->data[0] << frame->data[1] << frame->data[2];

    // 创建AVPacket
    pkt = av_packet_alloc();
    if (!pkt) {
        qDebug() << "av_packet_alloc error";
        goto end;
    }

    // 打开文件
    if (!inFile.open(QFile::ReadOnly)) {
        qDebug() << "file open error" << in.filename;
        goto end;
    }
    if (!outFile.open(QFile::WriteOnly)) {
        qDebug() << "file open error" << outFilename;
        goto end;
    }

    // frame->linesize[0]是缓冲区的大小
    // 读取文件数据
    while ((ret = inFile.read((char *) frame->data[0],
                              imgSize)) > 0) {
        // 编码
        if (encode(ctx, frame, pkt, outFile) < 0) {
            goto end;
        }

        //设置帧的序号
        frame->pts++;
    }

    // flush编码器
    encode(ctx, nullptr, pkt, outFile);

end:
    // 关闭文件
    inFile.close();
    outFile.close();

//    av_freep(&buf);
    // 释放资源
    if(frame){
        av_freep(&frame->data[0]);
        //av_freep相当于执行下面两行代码
//        av_free(frame->data[0]);
//        frame->data[0] = nullptr;
        av_frame_free(&frame);
    }
    av_packet_free(&pkt);
    avcodec_free_context(&ctx);
}
