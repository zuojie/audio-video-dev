#ifndef FFMPEGUTILS_H
#define FFMPEGUTILS_H

#include <QDebug>
#include <QFile>

extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
    #include <libavutil/imgutils.h>
}

typedef struct {
    const char *filename;
    int width;
    int height;
    AVPixelFormat format;
    int fps;
} VideoEncodeSpec;

class FFmpegUtils
{
public:
    FFmpegUtils();
    static void h264Encode(VideoEncodeSpec &in,
                          const char *outFilename);
};

#endif // FFMPEGUTILS_H

