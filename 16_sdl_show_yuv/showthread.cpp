#include "showthread.h"
#include <SDL2/SDL.h>
#include <QFile>

#ifdef Q_OS_WIN
    #define FILENAME "../test/in.yuv"
#else
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/in.yuv"
#endif

// 出错了就执行goto end
#define END(judge, func) \
    if (judge) { \
        qDebug() << #func << "Error" << SDL_GetError(); \
        goto end; \
    }

#define PIXEL_FORMAT SDL_PIXELFORMAT_IYUV
#define IMG_W 512
#define IMG_H 512

ShowThread::ShowThread(QObject *parent) : QThread(parent){
    connect(this,&ShowThread::finished,&ShowThread::deleteLater);
}

ShowThread::~ShowThread(){
    disconnect();
    requestInterruption();
    quit();
    wait();
    qDebug() << this << "析构了";
}

void ShowThread::run(){
    //定义变量
    // 窗口
    SDL_Window *window = nullptr;
    // 渲染上下文
    SDL_Renderer *renderer = nullptr;
    // 纹理（直接跟特定驱动程序相关的像素数据）
    SDL_Texture *texture = nullptr;

    // 文件
    QFile file(FILENAME);

    // 初始化Video子系统
    END(SDL_Init(SDL_INIT_VIDEO),SDL_Init);

    // 创建窗口
    window  = SDL_CreateWindow(
                // 窗口标题
                "SDL显示YUV图片",
                // 窗口的x坐标（未定义）
                SDL_WINDOWPOS_UNDEFINED,
                // 窗口的y坐标（未定义）
                SDL_WINDOWPOS_UNDEFINED,
                // 窗口宽度（跟图片宽度一样）
                IMG_W,
                // 窗口高度（跟图片高度一样）
                IMG_H,
                // SDL_WindowFlags 枚举，设置窗口显示样式效果
                SDL_WINDOW_SHOWN);

    // 创建渲染上下文（默认的渲染目标是window）
    renderer = SDL_CreateRenderer(window,
                       // 要初始化的渲染设备的索引，设置 -1 则初始化第一个支持 flags 的设备
                       -1,
                       SDL_RENDERER_ACCELERATED |
                       SDL_RENDERER_PRESENTVSYNC);
    if (!renderer) { // 说明开启硬件加速失败
        renderer = SDL_CreateRenderer(window, -1, 0);
    }
    END(!renderer, SDL_CreateRenderer);

    // 创建纹理
    texture = SDL_CreateTexture(renderer,
                                //显示的像素数据格式，我们显示的YUV图片像素格式是yuv420p，
                                //其实SDL_PIXELFORMAT_IYUV就是yuv420p像素格式
                                PIXEL_FORMAT,
                                //之前我们把同一个texture在窗口绘制多次时，我们设置的是SDL_TEXTUREACCESS_TARGET，
                                //这里我们设置SDL_TEXTUREACCESS_STATIC，当然设置成SDL_TEXTUREACCESS_STREAMING也可以
                                SDL_TEXTUREACCESS_STATIC,
                                IMG_W,IMG_H);
    END(!texture, SDL_CreateTexture);

    if(!file.open(QFile::ReadOnly)){
        qDebug() << "file open error" << FILENAME;
        goto end;
    }

    // 将YUV的像素数据填充到texture
    END(SDL_UpdateTexture(texture,
                          nullptr,// SDL_Rect:更新像素的矩形区域，传nullptr表示更新整个纹理区域
                          file.readAll().data(),// 原始像素数据
                          IMG_W),// 一行像素数据的字节数，这里传图片宽度即可
        SDL_UpdateTexture);
    // 渲染
    // 设置绘制颜色（这里随便设置了一个颜色：黄色）
    END(SDL_SetRenderDrawColor(renderer,255,255,0,SDL_ALPHA_OPAQUE),SDL_SetRenderDrawColor);

    // 用DrawColor清除渲染目标
    END(SDL_RenderClear(renderer),SDL_RenderClear);

    // 复制纹理到渲染目标上（默认是window）可以使用SDL_SetRenderTarget()修改渲染目标
    // srcrect源矩形框，dstrect目标矩形框，两者都传nullptr表示整个纹理渲染到整个目标上去
    END(SDL_RenderCopy(renderer,texture,nullptr,nullptr),SDL_RenderCopy);

    // 将此前的所有需要渲染的内容更新到屏幕上
    SDL_RenderPresent(renderer);

    // 延迟3秒退出
//    SDL_Delay(3000);

    while (!isInterruptionRequested()) {
        SDL_Event event;
        SDL_WaitEvent(&event);
        switch (event.type) {
            case SDL_QUIT:
                goto end;
        }
    }
end:
    file.close();
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

