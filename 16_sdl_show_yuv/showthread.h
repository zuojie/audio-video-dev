#ifndef SHOWBMPTHREAD_H
#define SHOWBMPTHREAD_H

#include <QThread>
#include <QDebug>

class ShowThread : public QThread
{
    Q_OBJECT
private:
    void run() override;
public:
    explicit ShowThread(QObject *parent = nullptr);
    ~ShowThread();
signals:

};

#endif // SHOWBMPTHREAD_H
