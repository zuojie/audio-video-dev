#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

#ifdef Q_OS_WIN
    #define FILENAME  "../test/out_640x480.yuv"
#else
    #define FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/out_640x480.yuv"
#endif

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow){
    ui->setupUi(this);

    // 创建播放器
    _player = new YuvPlayer(this);
    // 设置播放器的位置和尺寸
    int w = 640;
    int h = 480;
    int x = (width() - w) >> 1;
    int y = (height() - h) >> 1;
    _player->setGeometry(x, y, w, h);

    // 设置需要播放的文件
    Yuv yuv = {
        FILENAME,
        640,480,
        AV_PIX_FMT_YUV420P,
        30
    };
    _player->setYuv(yuv);

    // 监听播放
    connect(_player,&YuvPlayer::stateChanged,this,&MainWindow::onPlayerStateChanged);
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::on_playButton_clicked(){
    if(_player->isPlaying()){// 正在播放
        _player->pause();
    }else{// 没有正在播放
        _player->play();
    }
}

void MainWindow::on_stopButton_clicked(){
    _player->stop();
}

void MainWindow::onPlayerStateChanged(){
    if(_player->getState() == YuvPlayer::Playing){
        ui->playButton->setText("暂停");
    }else{// 没有正在播放
        ui->playButton->setText("播放");
    }
}
