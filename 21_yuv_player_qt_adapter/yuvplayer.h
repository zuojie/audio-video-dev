#ifndef YUVPLAYER_H
#define YUVPLAYER_H

#include <QWidget>
#include <QFile>

extern "C"{
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
}

typedef struct{
    const char *filename;
    int width;
    int height;
    AVPixelFormat pixelFormat;
    int fps;
}Yuv;

class YuvPlayer : public QWidget{
    Q_OBJECT
public:
    // 状态
    typedef enum{
        Stopped = 0,
        Playing,
        Paused,
        Finished
    } State;
    explicit YuvPlayer(QWidget *parent = nullptr);
    ~YuvPlayer();

    void play();
    void pause();
    void stop();
    bool isPlaying();
    void setYuv(Yuv &yuv);
    State getState();

signals:
    void stateChanged();

private:
    QFile *_file = nullptr;
    int _timerId = 0;// 先写一个0，否则有可能是个垃圾值
    State _state =  Stopped;
    Yuv _yuv;
    bool _playing;
    QImage *_currentImage =  nullptr;
    // 视频大小
    QRect _dstRect;
    // 刷帧的时间间隔
    int _interval;
    // 一帧图片的大小
    int _imgSize = 0;

    void timerEvent(QTimerEvent *event);
    void paintEvent(QPaintEvent *event);
    void freeCurrentImage();
    // 改变状态
    void setState(State state);

    void stopTimer();

    void closeFile();
};


#endif // YUVPLAYER_H
