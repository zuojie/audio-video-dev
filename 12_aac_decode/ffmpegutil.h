#ifndef FFMPEGUTIL_H
#define FFMPEGUTIL_H

#include <QDebug>
#include <QFile>

extern "C" {
    #include <libavcodec/avcodec.h>
    #include <libavutil/avutil.h>
}

typedef struct {
    const char *filename;
    int sampleRate;
    AVSampleFormat sampleFmt;
    int chLayout;
} AudioDecodeSpec;

class FFmpegUtil
{
public:
    FFmpegUtil();
    static void aacDecode(const char *inFilename,
                          AudioDecodeSpec &out);
};

#endif // FFMPEGUTIL_H
