#include "audiothread.h"

// 格式名称、设备名称目前暂时使用宏定义固定死
#ifdef Q_OS_WIN
    #define IN_FILENAME "../test/in.aac"
    #define OUT_FILENAME "../test/out.pcm"
#else
    #define IN_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/in.aac"
    #define OUT_FILENAME "/Users/zuojie/QtProjects/audio-video-dev/test/out.pcm"
#endif

AudioThread::AudioThread(QObject *parent):QThread(parent)
{
    // 当监听到线程结束时（finished），就调用deleteLater回收内存
    connect(this, &AudioThread::finished,
                this, &AudioThread::deleteLater);
}

AudioThread::~AudioThread(){
    // 断开所有的连接
    disconnect();
    // 内存回收之前，正常结束线程
    requestInterruption();
    // 安全退出
    quit();
    wait();
    qDebug() << this << "析构（内存被回收）";
}

void AudioThread::run(){
    AudioDecodeSpec out;
    out.filename = OUT_FILENAME;

    FFmpegUtil::aacDecode(IN_FILENAME,out);
    // 44100
    qDebug() <<"采样率："<<out.sampleRate;
    // s16
    qDebug() <<"采样格式："<<av_get_sample_fmt_name(out.sampleFmt);
    // 2
    qDebug() <<"声道数："<<av_get_channel_layout_nb_channels(out.chLayout);
}
