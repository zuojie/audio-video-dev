#ifndef SHOWBMPTHREAD_H
#define SHOWBMPTHREAD_H

#include <QThread>
#include <QDebug>
#include <SDL2/SDL.h>

class ShowBmpThread : public QThread
{
    Q_OBJECT
private:
    void run() override;
    SDL_Texture *createTexture(SDL_Renderer *renderer);
    void showClick(SDL_Event &event,
                               SDL_Renderer *renderer,
                               SDL_Texture *texture);
public:
    explicit ShowBmpThread(QObject *parent = nullptr);
    ~ShowBmpThread();
signals:

};

#endif // SHOWBMPTHREAD_H
